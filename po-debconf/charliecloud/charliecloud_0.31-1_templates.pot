# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the charliecloud package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: charliecloud\n"
"Report-Msgid-Bugs-To: charliecloud@packages.debian.org\n"
"POT-Creation-Date: 2020-07-05 14:56+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../charliecloud-common.templates:1001
msgid "Unprivileged user namespaces are disabled in the running kernel"
msgstr ""

#. Type: note
#. Description
#: ../charliecloud-common.templates:1001
msgid ""
"To use Charliecloud unprivileged user namespaces need to be enabled. This is "
"done by running the following commands as root:"
msgstr ""

#. Type: note
#. Description
#: ../charliecloud-common.templates:1001
msgid ""
"echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/00-local-userns."
"conf"
msgstr ""

#. Type: note
#. Description
#: ../charliecloud-common.templates:1001
msgid "systemctl restart procps"
msgstr ""
