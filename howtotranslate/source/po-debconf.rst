po-debconf
==========

(po-debconf text)

What is a debconf po-file?
--------------------------

1. Choose a package
-------------------

2. Make sure no one else already sent in a translation
------------------------------------------------------

3. Make sure that no one else already started to translate it
-------------------------------------------------------------

4. Announce your intention to translate the package
---------------------------------------------------

5. Download the pot-file
------------------------

6. Edit and save as <lang>.po
-----------------------------

7. Check for errors
-------------------

8. Send a request for review
----------------------------

9. Edit more if needed
----------------------

10. Send in a bug-report
------------------------

11. Send the bug ID to the mailing list.
----------------------------------------
